"""
Main program to train and select the winners
"""
__author__ = 'alainledon'

from referencedata import ReferenceData
import argparse

NFL_DATA_PATH = "~/gitdev/bitbucket.org/littlea1/mlkaggle/nfl/data/lookup/"


class NFLPicker(object):
    """
    """
    pass


def train_system():
    """
    """
    pass

def check_results():
    """
    """
    pass

def use_system():
    """
    """
    pass


def main():
    """
    Main entry point
    """
    lookups = ReferenceData(NFL_DATA_PATH)

if __name__ == "__main__":
    main()